Object.defineProperty(Object.prototype, "isEqual", {
  value: function(b) {
    return JSON.stringify(this) === JSON.stringify(b);
  }
});
