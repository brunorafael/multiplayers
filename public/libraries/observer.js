export class Observer {
  constructor() {
    this._observers = [];
  }

  getObservers() {
    return this._observers.slice();
  }

  addObserver(f) {
    this._observers.push(f);
  }

  removeObserver(f) {
    this._observers = this._observers.filter(obs => obs !== f);
  }

  notify(d) {
    this._observers.forEach(obs => obs(d));
  }
}
