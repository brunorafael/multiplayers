import { Element } from "./element.js";

export class Player extends Element {
  constructor({ name, position }) {
    super({ name, position });
  }
}
