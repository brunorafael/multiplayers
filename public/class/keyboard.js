import { Observer } from "../libraries/observer.js";

export class KeyBoard extends Observer {
  constructor(eventType, document) {
    super();

    if (!["keydown", "keyup", "keypress"].includes(eventType)) {
      throw new Error(
        `Type not accepted [${eventType}], events accepteds: keydown, keyup and keypress`
      );
    }

    this._eventType = eventType;
    this._document = document;
    this.init();
  }

  init() {
    this._document.addEventListener(
      this._eventType,
      this.keyBoardListener.bind(this)
    );
  }

  keyBoardListener(event) {
    const keyPressed = event.key.toLowerCase();
    // this.notify({ playerId: "_idx001", keyPressed: keyPressed });
  }
}
