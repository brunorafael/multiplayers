import { Observer } from "../libraries/observer.js";
import { Player } from "./player.js";
import { Fruit } from "./fruit.js";

export class Game extends Observer {
  constructor({ name, dimension }) {
    super();
    this._name = name;
    this._dimension = dimension;
    this.state = {
      sequences: 1,
      players: {},
      fruits: {},
      screen: this._dimension,
    };
  }
  setState(state) {
    this.state = state;
  }

  getState() {
    return this.state;
  }

  destroy() {
    this.state = {
      players: {},
      fruits: {},
    };
  }

  getPlayer(playerId) {
    return this.getState().players[playerId];
  }

  addPlayer(player) {
    if (player) {
      this.getState().players[player.id] = new Player(player);
    }
  }

  removePlayer(_idx) {
    if (_idx) {
      delete this.getState().players[_idx];
    }
  }

  addFruit(fruit) {
    if (fruit) {
      this.getState().fruits[this.getSequence()] = new Fruit(fruit);
    }
  }

  getSequence() {
    return "_idx00" + this.getState().sequences++;
  }

  removeFruit(_idx) {
    if (_idx) {
      delete this.getState().fruits[_idx];
    }
  }

  moveElement({ playerId, keyPressed }) {
    const _screen = this.getState().screen;
    const _player = this.getPlayer(playerId);

    const playerPosition = _player.position;

    if (playerPosition && keyPressed.match(/^(arrow)/)) {
      const keyPress = keyPressed.replace("arrow", "");

      const goTo = {
        up: playerPosition.y - 1,
        right: playerPosition.x + 1,
        down: playerPosition.y + 1,
        left: playerPosition.x - 1,
      };

      const postCurrent = goTo[keyPress] + 1;

      if (postCurrent <= 0 || postCurrent > _screen.width) {
        return;
      }

      switch (keyPress) {
        case "up":
        case "down":
          playerPosition.y = goTo[keyPress];
          break;
        case "right":
        case "left":
          playerPosition.x = goTo[keyPress];
          break;
      }

      this.checkFruitCollision(_player);
    }
  }

  checkFruitCollision(player) {
    const _fruits = this.getState().fruits;

    for (let _idx in _fruits) {
      if (_fruits.hasOwnProperty(_idx)) {
        const fruit = _fruits[_idx];
        const fruitPosition = fruit.position;
        const playerPosition = player.position;

        if (fruitPosition.isEqual(playerPosition)) {
          this.removeFruit(_idx);
        }
      }
    }
  }
}
