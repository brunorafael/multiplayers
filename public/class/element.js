export class Element {
  constructor({ name, position }) {
    if (this.constructor === Element) {
      throw new Error('Abstract class "Element" cannot be instantied');
    }

    this.name = name;
    this.position = position;
  }

  getName() {
    return this.name;
  }

  getPosition() {
    return this.position;
  }
}
