import { Element } from "./element.js";

export class Fruit extends Element {
  constructor({ name, position }) {
    super({ name, position });
  }
}
