export class Screen {
  constructor(target, game) {
    this._target = target;
    this._context = this._target.getContext("2d");
    this._game = game;
  }

  getTarget() {
    return this._target;
  }

  getContext() {
    return this._context;
  }

  getCurrentPlayer() {
    return this._currentPlayer;
  }

  setCurrentPlayer(player) {
    this._currentPlayer = player;
  }

  renderScreen() {
    const { players, fruits } = this._game.getState();
    this.getContext().clearRect(
      0,
      0,
      this.getTarget().width,
      this.getTarget().height
    );

    if (Object.keys(players).length) {
      this.createElements(players, { color: "#000" });
    }

    if (Object.keys(fruits).length) {
      this.createElements(fruits, { color: "#0E0" });
    }
  }

  createElements(elements, { color }) {
    if (elements) {
      for (let prop in elements) {
        if (elements.hasOwnProperty(prop)) {
          const _settings = Object.assign(elements[prop].position, {
            _wh: 1,
            _ht: 1,
          });

          this.getCurrentPlayer() !== prop
            ? (this.getContext().fillStyle = color)
            : (this.getContext().fillStyle = "#E0DB4E");
          this.getContext().fillRect(...Object.values(_settings));
        }
      }
    }
  }
}
