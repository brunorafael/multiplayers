import express from "express";
import http from "http";
import "./public/libraries/plugins.js";
import socketio from "socket.io";
import { Game } from "./public/class/game.js";

const port = process.env.PORT || 3000;
const app = express();
const server = http.createServer(app);
const sockets = socketio(server);

app.use(express.static("public"));

const game = new Game({
  name: "gameServer",
  dimension: { width: 10, height: 10 },
});

game.addFruit({ name: "cereja", position: { x: 5, y: 3 } });
game.addFruit({ name: "banane", position: { x: 2, y: 7 } });

sockets.on("connection", (socket) => {
  const _randPosition = Math.floor(
    Math.random() * game.getState().screen.width
  );

  socket.on("login", (name) => {
    socket.name = name;

    if (socket.name) {
      game.addPlayer({
        id: socket.id,
        name: socket.name,
        position: { x: _randPosition, y: _randPosition },
      });

      sockets.emit("stateGame", game.state);
      sockets.emit("connected", socket.id);
      console.log(`> ${socket.name} joined on the server.`);
    }
  });

  socket.on("onMovePlayer", (command) => {
    game.moveElement(command);

    if (socket.id !== game.state.players[socket.id]) {
      sockets.emit("stateGame", game.state);
    }
  });

  socket.on("disconnect", () => {
    if (socket.name && socket.id) {
      game.removePlayer(socket.id);

      sockets.emit("stateGame", game.state);
      sockets.emit("disconnected", socket.name);

      console.log(`${socket.name} has disconnected from the server.`);
    }
  });
});

server.listen(port, () => {
  console.log(`> Server listening on port: ${port}`);
});
